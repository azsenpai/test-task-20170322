<?php if (!empty($error)): ?>
    <p class="alert alert-danger"><?= $error ?></p>
<?php endif ?>

<form method="post">
    <div class="form-group">
        <label for="user-username">Имя пользователя</label>
        <input class="form-control" name="User[username]" id="user-username" placeholder="Имя пользователя">
    </div>

    <div class="form-group">
        <label for="user-password">Пароль</label>
        <input type="password" class="form-control" name="User[password]" id="user-password" placeholder="Пароль">
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Войти</button>
    </div>
</form>

<p>Вы можете войти в систему с помощью <strong>demo/demo</strong></p>
