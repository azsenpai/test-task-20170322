<p class="text-left">
    <a href="/" class="btn btn-sm btn-success">Главная</a>
    <a href="/user/logout" class="btn btn-sm btn-danger">Выход (<?= $user['username'] ?>)</a>
</p>

<?php if ($error): ?>
    <p class="alert alert-danger"><?= $error ?></p>
<?php endif ?>

<p>Ваш баланс: <?= number_format($user['balance'], 2, '.', ' ') ?></p>

<form method="post">
    <div class="form-group">
        <label for="user-balance">Вывести сумму</label>
        <input class="form-control" name="User[balance]" id="user-balance" placeholder="Вывести сумму">
    </div>

    <button type="submit" class="btn btn-primary">Вывести</button>
</form>