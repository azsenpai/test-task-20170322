<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Test-task</title>
        <link rel="stylesheet" href="/css/bootstrap.css">
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <div class="main">
            <?= $content ?>
        </div>
    </body>
</html>