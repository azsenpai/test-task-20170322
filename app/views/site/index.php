<?php if ($user === false): ?>
    <p class="alert alert-danger">Вы не авторизованы.</p>
    <a href="/user/login" class="btn btn-success">Войти <span class="glyphicon glyphicon-log-in"></span></a>
<?php else: ?>
    <p class="text-right">
        <a href="/user/logout" class="btn btn-sm btn-danger">Выход (<?= $user['username'] ?>)</a>
    </p>

    <p class="alert alert-info">Ваш баланс: <?= number_format($user['balance'], 2, '.', ' ') ?></p>
    <p>
        <a href="/user/add-balance" class="btn btn-success">Пополнить</a>
        <a href="/user/sub-balance" class="btn btn-primary">Вывести</a>
    </p>
<?php endif ?>
