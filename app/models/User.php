<?php

namespace app\models;

use PDO;
use core\base\Session;
use core\base\Model;
use core\helpers\ArrayHelper;

/**
 *
 */
class User extends Model
{
    /**
     * @var mixed
     */
    private static $_user;

    /**
     * @var void
     */
    public static function addBalance($user_id, $value)
    {
        $db = static::getDB();

        try {
            $db->beginTransaction();

            $st = $db->prepare('UPDATE user SET balance = ROUND(balance + ?, 2) WHERE id = ?');
            $st->execute([$value, $user_id]);

            $st = $db->prepare('INSERT INTO history(user_id, type, value) VALUES(?, ?, ?)');
            $st->execute([$user_id, 'add', $value]);

            $db->commit();
        } catch (\PDOException $e) {
            $db->rollBack();
        }
    }

    /**
     * @var boolean
     */
    public static function subBalance($user_id, $value)
    {
        $db = static::getDB();

        $result = false;

        try {
            $db->beginTransaction();

            $st = $db->prepare('SELECT balance FROM user WHERE id = ? FOR UPDATE');
            $st->execute([$user_id]);

            $balance = $st->fetchColumn();

            if ($balance === false) {
                throw new \Exception('user not found');
            }

            $max_balance = round($balance / 1.01, 2);

            if ($max_balance < $value) {
                throw new \Exception('balance not enough');
            }

            $commission = round($value * 0.01, 2);

            $st = $db->prepare('UPDATE user SET balance = ROUND(balance - ?, 2) WHERE id = ?');

            $st->execute([$value, $user_id]);
            $st->execute([$commission, $user_id]);

            $st = $db->prepare('INSERT INTO history(user_id, type, value) VALUES(?, ?, ?)');

            $st->execute([$user_id, 'sub', $value]);
            $st->execute([$user_id, 'sub', $commission]);

            $result = true;

            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public static function getUser()
    {
        $user_id = Session::get('user_id');

        if (empty($user_id)) {
            return false;
        }

        if (isset(static::$_user)) {
            return static::$_user;
        }

        $db = static::getDB();

        $st = $db->prepare('SELECT * FROM user WHERE id = ?');
        $st->execute([$user_id]);

        $result = $st->fetch(PDO::FETCH_ASSOC);

        if (empty($result)) {
            return false;
        } else {
            static::$_user = $result;
        }

        return static::$_user;
    }

    /**
     * @return string
     */
    public static function passwordHash($password)
    {
        return md5($password);
    }

    /**
     * @return boolean
     */
    public static function login($username, $password)
    {
        $db = static::getDB();

        $st = $db->prepare('SELECT * FROM user WHERE username = ?');
        $st->execute([$username]);

        $user = $st->fetch(PDO::FETCH_ASSOC);

        if (empty($user)) {
            return false;
        }

        if ($user['password'] != User::passwordHash($password)) {
            return false;
        }

        Session::set('user_id', $user['id']);

        return true;
    }

    /**
     * @return boolean
     */
    public static function logout()
    {
        return Session::destroy();
    }
}
