<?php

namespace app\models;

use core\base\Model;

/**
 *
 */
class Migration extends Model
{
    /**
     * @return void
     */
    public function createUserTable()
    {
        $db = static::getDB();

        $sql = 'CREATE TABLE IF NOT EXISTS user(
            id INT NOT NULL AUTO_INCREMENT,
            PRIMARY KEY(id),
            username VARCHAR(255) NOT NULL,
            password VARCHAR(255) NOT NULL,
            balance DOUBLE NOT NULL
        )';

        $db->prepare($sql)->execute();

        $sql = 'CREATE INDEX `idx-user-username`
            ON user(username)';

        $db->prepare($sql)->execute();
    }

    /**
     * @return void
     */
    public function createHistoryTable()
    {
        $db = static::getDB();

        $sql = 'CREATE TABLE IF NOT EXISTS history(
            id INT NOT NULL AUTO_INCREMENT,
            PRIMARY KEY(id),
            user_id INT NOT NULL,
            FOREIGN KEY (user_id) REFERENCES user(id),
            type VARCHAR(255) NOT NULL,
            value DOUBLE NOT NULL,
            created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
        )';

        $db->prepare($sql)->execute();

        $sql = 'CREATE INDEX `idx-history-user_id`
            ON history(user_id)';

        $db->prepare($sql)->execute();
    }
}
