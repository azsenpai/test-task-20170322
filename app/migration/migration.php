<?php

require dirname(__DIR__) . '/../core/bootstrap.php';

$migration = new app\models\Migration();

$migration->createUserTable();
$migration->createHistoryTable();
