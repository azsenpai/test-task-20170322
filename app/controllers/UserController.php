<?php

namespace app\controllers;

use core\base\Controller;
use core\helpers\ArrayHelper;
use app\models\User;

/**
 *
 */
class UserController extends Controller
{
    /**
     *
     */
    public function actionLogin()
    {
        if (User::getUser()) {
            return $this->goHome();
        }

        $error = null;

        if (!empty($_POST['User'])) {
            $username = ArrayHelper::getValue($_POST['User'], 'username');
            $password = ArrayHelper::getValue($_POST['User'], 'password');

            if (!User::login($username, $password)) {
                $error = 'Неправильное имя пользователя или пароль.';
            } else {
                return $this->goHome();
            }
        }

        return $this->render('login', [
            'error' => $error,
        ]);
    }

    /**
     *
     */
    public function actionLogout()
    {
        User::logout();

        return $this->goHome();
    }

    /**
     *
     */
    public function actionAddBalance()
    {
        $user = User::getUser();

        if (!$user) {
            return $this->goHome();
        }

        if (!empty($_POST['User'])) {
            $balance = ArrayHelper::getValue($_POST['User'], 'balance');
            $balance = str_replace(' ', '', $balance);

            if (is_numeric($balance) && $balance > 0) {
                User::addBalance($user['id'], round($balance, 2));

                return $this->redirect('/user/add-balance?success=1');
            }
        }

        return $this->render('add-balance', [
            'user' => $user,
        ]);
    }

    /**
     *
     */
    public function actionSubBalance()
    {
        $user = User::getUser();

        if (!$user) {
            return $this->goHome();
        }

        $error = null;

        if (!empty($_POST['User'])) {
            $balance = ArrayHelper::getValue($_POST['User'], 'balance');
            $balance = str_replace(' ', '', $balance);

            if (is_numeric($balance) && $balance > 0) {
                $balance = round($balance, 2);
                $max_balance = round($user['balance'] / 1.01, 2);

                if ($balance > $max_balance) {
                    $error = 'Максимальная сумма которую вы можете вывести: ' . number_format($max_balance, 2, '.', ' ');
                } else {
                    User::subBalance($user['id'], round($balance, 2));

                    return $this->redirect('/user/sub-balance');
                }
            }
        }

        return $this->render('sub-balance', [
            'error' => $error,
            'user' => $user,
        ]);
    }

    /**
     *
     */
    public function actionQueue()
    {
        $user = User::getUser();

        if (!$user) {
            return $this->goHome();
        }

        $queue = BalanceQueue::getAll();

        return $this->render('queue', [
            'queue' => $queue,
            'user' => $user,
        ]);
    }
}
