<?php

namespace app\controllers;

use core\base\Controller;
use app\models\User;

/**
 *
 */
class SiteController extends Controller
{
    /**
     *
     */
    public function actionIndex()
    {
        $user = User::getUser();

        return $this->render('index', [
            'user' => $user,
        ]);
    }
}
