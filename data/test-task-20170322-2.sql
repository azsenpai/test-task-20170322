-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               10.1.21-MariaDB - mariadb.org binary distribution
-- Операционная система:         Win32
-- HeidiSQL Версия:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных test-task-20170322
CREATE DATABASE IF NOT EXISTS `test-task-20170322` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `test-task-20170322`;

-- Дамп структуры для таблица test-task-20170322.balance_queue
CREATE TABLE IF NOT EXISTS `balance_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` double NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx-balance_queue-user_id` (`user_id`),
  CONSTRAINT `balance_queue_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы test-task-20170322.balance_queue: ~20 rows (приблизительно)
DELETE FROM `balance_queue`;
/*!40000 ALTER TABLE `balance_queue` DISABLE KEYS */;
INSERT INTO `balance_queue` (`id`, `user_id`, `type`, `value`, `status`, `created_at`) VALUES
	(21, 1, 'sub', 90, 1, '2017-03-25 19:56:31'),
	(22, 1, 'sub', 90, 2, '2017-03-25 19:56:32'),
	(23, 1, 'add', 90.9, 1, '2017-03-25 19:57:51'),
	(24, 1, 'add', 200, 1, '2017-03-25 20:09:52'),
	(25, 1, 'add', 700, 1, '2017-03-25 20:16:15'),
	(26, 1, 'sub', 900, 1, '2017-03-25 20:16:51'),
	(27, 1, 'sub', 900, 2, '2017-03-25 20:16:52');
/*!40000 ALTER TABLE `balance_queue` ENABLE KEYS */;

-- Дамп структуры для таблица test-task-20170322.history
CREATE TABLE IF NOT EXISTS `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` double NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx-history-user_id` (`user_id`),
  CONSTRAINT `history_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы test-task-20170322.history: ~0 rows (приблизительно)
DELETE FROM `history`;
/*!40000 ALTER TABLE `history` DISABLE KEYS */;
INSERT INTO `history` (`id`, `user_id`, `type`, `value`, `created_at`) VALUES
	(1, 1, 'sub', 90, '2017-03-25 19:56:57'),
	(2, 1, 'sub', 0.9, '2017-03-25 19:56:57'),
	(3, 1, 'add', 90.9, '2017-03-25 19:58:00'),
	(4, 1, 'add', 200, '2017-03-25 20:16:27'),
	(5, 1, 'add', 700, '2017-03-25 20:16:27'),
	(6, 1, 'sub', 900, '2017-03-25 20:17:00'),
	(7, 1, 'sub', 9, '2017-03-25 20:17:00');
/*!40000 ALTER TABLE `history` ENABLE KEYS */;

-- Дамп структуры для таблица test-task-20170322.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `balance` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx-user-username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы test-task-20170322.user: ~1 rows (приблизительно)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`, `balance`) VALUES
	(1, 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 91);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Дамп структуры для таблица test-task-20170322.variable
CREATE TABLE IF NOT EXISTS `variable` (
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы test-task-20170322.variable: ~1 rows (приблизительно)
DELETE FROM `variable`;
/*!40000 ALTER TABLE `variable` DISABLE KEYS */;
INSERT INTO `variable` (`key`, `value`) VALUES
	('balance_queue_status', NULL);
/*!40000 ALTER TABLE `variable` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
