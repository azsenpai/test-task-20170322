<?php

namespace core\helpers;

/**
 *
 */
class Url
{
    /**
     * @return string
     */
    public static function normalize($url)
    {
        if (strlen($url) > 1) {
            $url = rtrim($url, '/');
        }

        return $url;
    }

    /**
     * @return string
     */
    public static function removeQueryString($url)
    {
        $pos = strpos($url, '?');

        if ($pos !== false) {
            $url = substr($url, 0, $pos);
        }

        return $url;
    }
}
