<?php

namespace core\helpers;

/**
 *
 */
class Inflector
{
    /**
     * @return string
     */
    public static function camelize($word)
    {
        return str_replace(' ', '', ucwords(preg_replace('/[^A-Za-z0-9]+/', ' ', $word)));
    }

    /**
     * @return string
     */
    public static function camel2id($name, $separator = '-', $strict = false)
    {
        $regex = $strict ? '/[A-Z]/' : '/(?<![A-Z])[A-Z]/';

        if ($separator === '_') {
            return trim(strtolower(preg_replace($regex, '_\0', $name)), '_');
        }

        return trim(strtolower(str_replace('_', $separator, preg_replace($regex, $separator . '\0', $name))), $separator);
    }

    /**
     * @return string
     */
    public static function getControllerName($name)
    {
        return self::camelize($name) . 'Controller';
    }

    /**
     * @return string
     */
    public static function getActionName($name)
    {
        return 'action' . self::camelize($name);
    }

    /**
     * @return string
     */
    public static function getControllerClass($name)
    {
        return 'app\\controllers\\' . self::getControllerName($name);
    }

    /**
     * @return string
     */
    public static function getClassName($class)
    {
        $parts = explode('\\', $class);

        return end($parts);
    }
}
