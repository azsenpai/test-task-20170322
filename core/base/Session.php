<?php

namespace core\base;

/**
 *
 */
class Session
{
    /**
     * @return mixed
     */
    public static function get($key, $default = null)
    {
        session_start();

        $result = $default;

        if (isset($_SESSION[$key])) {
            $result = $_SESSION[$key];
        }

        session_write_close();

        return $result;
    }

    /**
     * @return void
     */
    public static function set($key, $value)
    {
        session_start();

        $_SESSION[$key] = $value;

        session_write_close();
    }

    /**
     * @return boolean
     */
    public static function destroy()
    {
        session_start();

        return session_destroy();
    }
}
