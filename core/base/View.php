<?php

namespace core\base;

/**
 *
 */
class View
{
    /**
     * @var string
     */
    public static $template = 'main';

    /**
     * @var array
     */
    public static $params = [];

    /**
     * @return string
     */
    public static function render($view, $params = [])
    {
        $filename = sprintf('%s/views/%s.php', APP_PATH, $view);

        if (!is_readable($filename)) {
            throw new \Exception("$filename not found");
        }

        ob_start();
        ob_implicit_flush(false);

        extract($params, EXTR_OVERWRITE);
        require($filename);

        return ob_get_clean();
    }

    /**
     *
     */
    public static function renderTemplate($view, $params = [])
    {
        $filename = sprintf('%s/views/layouts/%s.php', APP_PATH, static::$template);

        if (!is_readable($filename)) {
            throw new \Exception("$filename not found");
        }

        $content = static::render($view, $params);

        ob_start();
        ob_implicit_flush(false);

        require($filename);

        return ob_get_clean();
    }
}
