<?php

require dirname(__DIR__) . '/vendor/autoload.php';

defined('CORE_PATH') or define('CORE_PATH', __DIR__);

defined('APP_PATH') or define('APP_PATH', dirname(CORE_PATH) . '/app');

error_reporting(E_ALL);

set_error_handler('core\base\Error::errorHandler');
set_exception_handler('core\base\Error::exceptionHandler');
