<?php

require dirname(__DIR__) . '/core/bootstrap.php';

$router = new core\base\Router();

$router->batchAdd([
    [
        'url' => '/',
        'params' => [
            'controller' => 'site',
            'action' => 'index',
        ],
    ],
    [
        'url' => '/user/login',
        'params' => [
            'controller' => 'user',
            'action' => 'login',
        ],
    ],
    [
        'url' => '/user/logout',
        'params' => [
            'controller' => 'user',
            'action' => 'logout',
        ],
    ],
    [
        'url' => '/user/add-balance',
        'params' => [
            'controller' => 'user',
            'action' => 'add-balance',
        ],
    ],
    [
        'url' => '/user/sub-balance',
        'params' => [
            'controller' => 'user',
            'action' => 'sub-balance',
        ],
    ],
]);

$router->dispatch($_SERVER['REQUEST_URI']);
